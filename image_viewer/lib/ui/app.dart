import 'package:flutter/material.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }

}


class AppState extends State<App> {
  int counter = 0;

  fetchImages() {
    counter++;
    print('Hello there! counter=$counter');
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {

    var appWidget = new MaterialApp(
        home: Scaffold(
          appBar: AppBar(title: Text('Image Viewer'),),
          body: Text('Display list of images here. counter=$counter'),
          floatingActionButton: FloatingActionButton(
            onPressed: fetchImages
          ),
        )
    );

    return appWidget;
  }
  
}