import 'dart:convert';
class ImageModel {
  int? id;
  String? url;

  ImageModel(this.id, this.url);

  ImageModel.fromJson(parsedJson) {
    id = parsedJson['id'];
    url = parsedJson['url'];
  }

  String toString() {
    return '($id, $url)';
  }
}

void main() {
  var jsonRaw = '{"id": 1, "url": "https://link.com/image"}';
  var jsonImage = json.decode(jsonRaw);

  var imageModel = ImageModel.fromJson(jsonImage);

  print(imageModel);
}
