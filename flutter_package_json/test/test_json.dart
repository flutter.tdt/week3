import 'dart:convert';
void main() {

  test1();
  test2();

}

test1() {
  var jsonRaw = '{"id": 1, "url": "https://link.com/image"}';
  JsonDecoder decoder = new JsonDecoder();
  var jsonObject = decoder.convert(jsonRaw);

  print(jsonObject['id']);
  print(jsonObject['url']);
}

test2() {
  var jsonRaw = '{"id": 1, "url": "https://link.com/image"}';

  var jsonObject = json.decode(jsonRaw);

  print(jsonObject['id']);
  print(jsonObject['url']);
}