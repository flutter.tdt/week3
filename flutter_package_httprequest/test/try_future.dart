import 'dart:async';

void main() {
  DateTime _now = DateTime.now();
  print('timestamp: ${_now.hour}:${_now.minute}:${_now.second}.${_now.millisecond}');
  print('Getting a message from a function');
  get('http://anystring.xyz').then(
          (result) {

        _now = DateTime.now();
        print('timestamp: ${_now.hour}:${_now.minute}:${_now.second}.${_now.millisecond}');
        print(result);
      }
  );

  print('After call get()...');
  _now = DateTime.now();
  print('timestamp: ${_now.hour}:${_now.minute}:${_now.second}.${_now.millisecond}');
}


Future<String> get(url) {
  return new Future.delayed(
      new Duration(seconds: 10), () {
    return 'This message taken 1 seconds to build. Then it returns late.';
  }
  );

}